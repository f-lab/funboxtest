var data = {'productUnit': {
        product_1: {
            mod: '',
            note: 'Сказочное заморское яство',
            title: 'Нямушка',
            subtitle: 'с фуа-гра',
            info: '<strong>10</strong> порций <br> мышь в подарок',
            weight: '0,5',
            weightText: 'кг',
            footerInfo: 'Печень утки разварная с артишоками.',
            footerDisabled: 'Печалька, с фуа-гра закончился.'
        },
        product_2: {
            mod: 'active',
            note: 'Сказочное заморское яство',
            title: 'Нямушка',
            subtitle: 'с рыбой',
            info: '<strong>40</strong> порций <br> <strong>2</strong> мыши в подарок',
            weight: '2',
            weightText: 'кг',
            footerInfo: 'Головы щучьи с чесноком да свежайшая сёмгушка.',
            footerDisabled: 'Печалька, с рыбой закончился.'
        },
        product_3: {
            mod: 'disabled',
            note: 'Сказочное заморское яство',
            title: 'Нямушка',
            subtitle: 'с курой',
            info: '<strong>100</strong> порций <br> <strong>5</strong> мышей в подарок <br> заказчик доволен',
            weight: '5',
            weightText: 'кг',
            footerInfo: 'Филе из цыплят с трюфелями в бульоне.',
            footerDisabled: 'Печалька, с курой закончился.'
        }
    }};

