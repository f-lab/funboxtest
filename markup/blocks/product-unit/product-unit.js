$('.js-product-unit').click(function () {
    let parents = $(this).parents('.product-unit');

    if (!parents.hasClass('product-unit_disabled')) {
        parents.removeClass('product-unit_warning');
        parents.toggleClass('product-unit_active');
    }

    return false;
});

$('.js-product-unit').on('mouseleave', function () {

    let parents = $(this).parents('.product-unit');

    if (parents.hasClass('product-unit_active')) {
        parents.addClass('product-unit_warning');
    }
});
